﻿namespace Day3TeamTasks
{
    public class FirstTask
    {
        public static bool HasSquareWithFibonacciNumbers(int[,] matrix)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);

            for (int i = 0; i < rows - 1; i++)
            {
                for (int j = 0; j < cols - 1; j++)
                {
                    int a = matrix[i, j];
                    int b = matrix[i, j + 1];
                    int c = matrix[i + 1, j + 1];
                    int d = matrix[i + 1, j];

                    if (IsFibonacciSequence(a, b, c, d))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsFibonacciSequence(int a, int b, int c, int d)
        {
            int[] numbers = GetFibonacciNumbers();

            for (int i = 0; i < numbers.Length - 3; i++)
            {
                if (a == numbers[i] && b == numbers[i + 1] 
                    && c == numbers[i + 2] && d == numbers[i + 3])
                {
                    return true;
                }
            }

            return false;
        }

        public static int[] GetFibonacciNumbers()
        {
            int[] fib = new int[100];

            fib[0] = 0;
            fib[1] = 1;

            for (int i = 2; i < 100; i++)
            {
                fib[i] = fib[i - 1] + fib[i - 2];
            }

            return fib;
        }
    }
}