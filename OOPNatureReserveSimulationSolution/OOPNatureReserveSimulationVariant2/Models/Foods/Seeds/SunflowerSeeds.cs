﻿namespace ReserveSimulation.Models.Foods.Seeds
{
    public class SunflowerSeeds : Seeds
    {
        private const int NUTRITIONAL_VALUE = 4;
        public SunflowerSeeds()
            : base(NUTRITIONAL_VALUE)
        {

        }
    }
}