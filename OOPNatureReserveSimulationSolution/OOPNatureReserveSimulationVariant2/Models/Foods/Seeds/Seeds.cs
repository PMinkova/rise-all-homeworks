﻿namespace ReserveSimulation.Models.Foods.Seeds
{
    public abstract class Seeds : Food
    {
        protected Seeds(int nutritionalValue)
            : base(nutritionalValue)
        {

        }
    }
}
