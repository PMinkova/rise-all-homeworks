﻿namespace ReserveSimulation.Models.Foods.Plants
{
    public class Palm : Plant
    {
        private const int NUTRITIONAL_VALUE = 4;
        private const int REGENERATE_NUTRITIONAL_VALUE = 2;

        public Palm()
            : base(NUTRITIONAL_VALUE, REGENERATE_NUTRITIONAL_VALUE)
        {

        }
    }
}