﻿namespace ReserveSimulation.Models.Foods.Plants
{
    public class Algae : Plant
    {
        private const int NUTRITIONAL_VALUE = 10;
        private const int REGENERATE_NUTRITIONAL_VALUE = 2;

        public Algae()
            : base(NUTRITIONAL_VALUE, REGENERATE_NUTRITIONAL_VALUE)
        {

        }
    }
}