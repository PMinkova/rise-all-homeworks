﻿namespace ReserveSimulation.Models.Foods.Plants
{
    public class Cabbage : Plant
    {
        private const int NUTRITIONAL_VALUE = 7;
        private const int REGENERATE_NUTRITIONAL_VALUE = 1;

        public Cabbage()
            : base(NUTRITIONAL_VALUE, REGENERATE_NUTRITIONAL_VALUE)
        {

        }
    }
}