﻿namespace ReserveSimulation.Models.Foods.Plants
{
    public abstract class Plant : Food
    {
        protected Plant(int nutritionalValue, int regenerateNutritionalValue)
            : base(nutritionalValue)
        {
            this.RegenerateNutritionalValue = regenerateNutritionalValue;
            this.InitialNutritionalValue = nutritionalValue;
        }

        public bool IsRegenerated { get; private set; } = true;

        protected int InitialNutritionalValue { get; }

        protected int RegenerateNutritionalValue { get; }

        public override void RegainNutrition(int nutritionalValueLeft)
        {
            this.IsRegenerated = false;
        }

        public void Regenerate()
        {
            if (this.NutritionalValue + this.RegenerateNutritionalValue <= this.InitialNutritionalValue)
            {
                this.NutritionalValue += this.RegenerateNutritionalValue;
            }
            else
            {
                this.NutritionalValue = this.InitialNutritionalValue;
                this.IsRegenerated = true;
            }
        }
    }
}