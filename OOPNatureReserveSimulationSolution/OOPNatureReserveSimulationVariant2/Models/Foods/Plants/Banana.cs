﻿namespace ReserveSimulation.Models.Foods.Plants
{
    public class Banana : Plant
    {
        private const int NUTRITIONAL_VALUE = 12;
        private const int REGENERATE_NUTRITIONAL_VALUE = 1;

        public Banana()
            : base(NUTRITIONAL_VALUE, REGENERATE_NUTRITIONAL_VALUE)
        {

        }
    }
}