﻿namespace ReserveSimulation.Models.Foods.Plants
{
    public class BlueBerry : Plant
    {
        private const int NUTRITIONAL_VALUE = 16;
        private const int REGENERATE_NUTRITIONAL_VALUE = 3;

        public BlueBerry()
            : base(NUTRITIONAL_VALUE, REGENERATE_NUTRITIONAL_VALUE)
        {

        }
    }
}