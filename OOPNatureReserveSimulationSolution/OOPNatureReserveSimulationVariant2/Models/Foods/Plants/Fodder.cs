﻿namespace ReserveSimulation.Models.Foods.Plants
{
    public class Fodder : Plant
    {
        private const int NUTRITIONAL_VALUE = 5;
        private const int REGENERATE_NUTRITIONAL_VALUE = 1;

        public Fodder()
            : base(NUTRITIONAL_VALUE, REGENERATE_NUTRITIONAL_VALUE)
        {

        }
    }
}