﻿namespace ReserveSimulation.Models.Foods.Plants
{
    public class Carrot : Plant
    {
        private const int NUTRITIONAL_VALUE = 6;
        private const int REGENERATE_NUTRITIONAL_VALUE = 3;

        public Carrot()
            : base(NUTRITIONAL_VALUE, REGENERATE_NUTRITIONAL_VALUE)
        {

        }
    }
}