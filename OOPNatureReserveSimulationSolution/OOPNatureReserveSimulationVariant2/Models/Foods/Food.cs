﻿namespace ReserveSimulation.Models.Foods
{
    public abstract class Food
    {
        protected Food(int nutritionalValue)
        {
            NutritionalValue = nutritionalValue;
        }

        public int NutritionalValue { get; protected set; }

        public virtual int DeliverNutrition()
        {
            int nutritionalValue = NutritionalValue;
            NutritionalValue = 0;
            return nutritionalValue;
        }

        public virtual void RegainNutrition(int nutritionalValueLeft)
        {
            NutritionalValue += nutritionalValueLeft;
        }
    }
}