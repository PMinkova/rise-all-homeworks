﻿namespace ReserveSimulation.Models.Map
{
    using Biomes;

    public class Map
    {
        private readonly int size;
        private readonly Tile[,] tiles;

        private static readonly List<Biome> Biomes = new List<Biome>() { new Jungle(), new Plain(), new Ocean(), new Forest() };

        public Map(int size)
        {
            this.size = size;
            this.tiles = new Tile[size, size];

            var random = new Random();

            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    var randomIndex = random.Next(Biomes.Count);
                    var randomBiome = Biomes[randomIndex];

                    tiles[x, y] = randomBiome.CreateTile(this);
                }
            }
        }

        public int Size => size;

        public Tile GetTile(int x, int y)
        {
            return tiles[x, y];
        }
    }

}
