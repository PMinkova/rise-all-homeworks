﻿namespace ReserveSimulation.Models.Map
{
    using System.Runtime.CompilerServices;
    using Animals;
    using Foods.Plants;
    using Foods;

    public class Tile
    {
        private HashSet<Animal> encounteredAnimals;

        public Tile(ICollection<Animal> animals, ICollection<Plant> plants)
        {
            this.Animals = animals.ToHashSet();
            this.Plants = plants.ToHashSet();
            this.encounteredAnimals = animals.ToHashSet();
            Foods.UnionWith(animals);
            Foods.UnionWith(plants);
        }

        public IReadOnlyCollection<Animal> EncounteredAnimals => this.encounteredAnimals;

        public HashSet<Animal> Animals { get; }

        public HashSet<Food> Foods { get; }

        public HashSet<Plant> Plants { get; }

        public void AddAnimal(Animal animal)
        {
            Animals.Add(animal);
            Foods.Add(animal);
        }

        public void RemoveAnimal(Animal animal)
        {
            Animals.Remove(animal);
            Foods.Remove(animal);
        }
    }
}
