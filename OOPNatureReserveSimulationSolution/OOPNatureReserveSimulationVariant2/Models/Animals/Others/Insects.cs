﻿namespace ReserveSimulation.Models.Animals.Others
{
    using System.Collections.Generic;
    using Animals;
    using Map;

    public class Insects : Animal
    {
        private const int MAX_ENERGY = 80;
        private const int NUTRITIONAL_VALUE = 10;

        public Insects(Map map) :
            base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "BlueBerry", };
    }
}