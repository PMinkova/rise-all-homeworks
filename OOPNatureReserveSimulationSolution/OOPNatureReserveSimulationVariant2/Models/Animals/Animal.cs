﻿namespace ReserveSimulation.Models.Animals
{
    using System.Text;

    using Map;
    using Foods;

    public abstract class Animal : Food
    {
        private const int StarvingThreshold = 1;
        private Map map;

        protected Animal(int maxEnergy, int nutritionalValue, Map map)
            : base(nutritionalValue)
        {
            this.map = map;
            this.CurrentEnergy = maxEnergy;
            this.MaxEnergy = maxEnergy;
            this.IsAlive = true;
        }

        public bool IsStarving => StarvingThreshold == this.CurrentEnergy;

        public bool IsAlive { get; protected set; }

        public int LifeSpan { get; protected set; }

        protected abstract ICollection<string> Diet { get; }

        protected int MaxEnergy { get; }

        protected bool HasGrown => this.LifeSpan == 10;

        public int CurrentEnergy { get; private set; }

        protected virtual string Eating(Food food)
        {
            return $"{this.GetType().Name} is eating {food.GetType().Name}";
        }

        protected virtual string Dying()
        {
            return $"{GetType().Name} has died.";
        }

        protected virtual string Starving()
        {
            return $"{GetType().Name}: I am starving! Give me some food, please!!!";
        }

        public virtual string Feed(Food food)
        {
            var feedMessages = new StringBuilder();

            if (this.IsAlive)
            {
                this.LifeSpan++;

                if (this.Diet.Any(f => f == food.GetType().Name))
                {
                    feedMessages.AppendLine(this.ExchangeNutrition(food));
                }
                else
                {
                    feedMessages.AppendLine(this.ReduceEnergy(food));
                }
            }

            return feedMessages.ToString().Trim();
        }

        private string ReduceEnergy(Food food)
        {
            var messages = new StringBuilder();

            this.CurrentEnergy--;

            if (food.NutritionalValue == 0)
            {
                messages.AppendLine($"The plant {food.GetType().Name} has no nutritional value");
            }
            else
            {
                messages.AppendLine($"{this.GetType().Name} does not eat {food.GetType().Name}");
            }

            if (this.CurrentEnergy <= 0)
            {
                this.IsAlive = false;
                messages.AppendLine(Dying());
            }

            return messages.ToString();
        }

        public override int DeliverNutrition()
        {
            this.IsAlive = false;
            return base.DeliverNutrition();
        }

        private string ExchangeNutrition(Food food)
        {
            var messages = new StringBuilder();

            messages.AppendLine(Eating(food));

            this.CurrentEnergy += food.DeliverNutrition();

            if (this.CurrentEnergy > this.MaxEnergy)
            {
                var nutritionalValueLeft = this.CurrentEnergy - this.MaxEnergy;
                this.CurrentEnergy = this.MaxEnergy;
                food.RegainNutrition(nutritionalValueLeft);
            }

            if (this.IsStarving)
            {
                messages.AppendLine(Starving());
            }

            return messages.ToString();
        }

        public string TryToMove(Tile tile, Random random)
        {
            var randomX = random.Next(map.Size);
            var randomY = random.Next(map.Size);

            var tileToMove = map.GetTile(randomX, randomY);

            if (tileToMove.EncounteredAnimals.Any(a => a.GetType().Name == this.GetType().Name))
            {
                tile.RemoveAnimal(this);
                tileToMove.AddAnimal(this);
                return $"{this.GetType().Name} has moved to another tile.";
            }

            return $"{this.GetType().Name} cannot find an appropriate tile.";
        }
    }
}