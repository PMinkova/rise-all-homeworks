﻿using ReserveSimulation.Models.Foods;

namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public class Monkey : Mammal
    {
        private const int MAX_ENERGY = 45;
        private const int NUTRITIONAL_VALUE = 7;

        public Monkey(Map map) 
            : base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } = 
            new HashSet<string> { "Banana", "BlueBerry" };

        protected override string Starving()
        {
            return "I want a banana!";
        }

        public override string Feed(Food food)
        {
            if (this.HasGrown)
            {
                this.Diet.Add("Palm");
            }

            return base.Feed(food);
        }
    }
}