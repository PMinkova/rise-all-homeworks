﻿namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public class Elk : Mammal
    {
        private const int MAX_ENERGY = 60;
        private const int NUTRITIONAL_VALUE = 6;

        public Elk(Map map) :
            base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Insects", "Grass", "Fodder", "BlueBerry" };
    }
}