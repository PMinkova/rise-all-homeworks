﻿namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public class Rabbit : Mammal
    {
        private const int MAX_ENERGY = 80;
        private const int NUTRITIONAL_VALUE = 10;

        public Rabbit(Map map) 
            : base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "SunflowerSeeds", "Banana", "BlueBerry", "Grass", "Carrot" };
    }
}