﻿namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public class Sheep : Mammal
    {
        private const int MAX_ENERGY = 80;
        private const int NUTRITIONAL_VALUE = 10;

        public Sheep(Map map) :
            base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }


        protected override ICollection<string> Diet { get; } 
            = new HashSet<string> { "BlueBerry", "Grass", "Fodder" };
    }
}