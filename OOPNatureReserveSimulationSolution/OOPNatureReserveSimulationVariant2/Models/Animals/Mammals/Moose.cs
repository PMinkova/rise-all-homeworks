﻿namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public class Moose : Mammal
    {
        private const int MAX_ENERGY = 102;
        private const int NUTRITIONAL_VALUE = 14;

        public Moose(Map map) 
            : base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Banana", "BlueBerry" };
    }
}