﻿using ReserveSimulation.Models.Foods;

namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public class Lion : Mammal
    {
        private const int MAX_ENERGY = 80;
        private const int NUTRITIONAL_VALUE = 10;

        public Lion(Map map) :
            base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Monkey", "Rabbit", "Mouse" };

        protected override string Eating(Food food)
        {
            return $"{GetType().Name}: Yummy";
        }

        public override string Feed(Food food)
        {
            if (this.HasGrown)
            {
                this.Diet.Add("Deer");
                this.Diet.Add("Moose");
                this.Diet.Add("Sheep");
            }

            return base.Feed(food);
        }
    }
}