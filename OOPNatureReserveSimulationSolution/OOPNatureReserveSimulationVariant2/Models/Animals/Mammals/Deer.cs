﻿namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public class Deer : Animal
    {

        private const int MAX_ENERGY = 85;
        private const int NUTRITIONAL_VALUE = 6;

        public Deer(Map map) 
            : base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Insects", "Grass", "Fodder", "BlueBerry" };
    }
}