﻿using ReserveSimulation.Models.Foods;

namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public class Mouse : Mammal
    {
        private const int MAX_ENERGY = 60;
        private const int NUTRITIONAL_VALUE = 4;

        public Mouse(Map map) 
            : base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override string Eating(Food food)
        {
            return "Cyrr cyrr";
        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "SunflowerSeeds", "Banana", "BlueBerry" };
    }
}