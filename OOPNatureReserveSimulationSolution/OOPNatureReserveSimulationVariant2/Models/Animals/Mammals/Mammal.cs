﻿namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public abstract class Mammal : Animal
    {
        protected Mammal(int maxEnergy, int nutritionalValue, Map map)
            : base(maxEnergy, nutritionalValue, map)
        {

        }
    }
}