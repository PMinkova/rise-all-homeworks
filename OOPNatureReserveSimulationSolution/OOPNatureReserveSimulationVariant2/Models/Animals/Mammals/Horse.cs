﻿using ReserveSimulation.Models.Foods;

namespace ReserveSimulation.Models.Animals.Mammals
{
    using Map;

    public class Horse : Mammal
    {
        private const int MAX_ENERGY = 90;
        private const int NUTRITIONAL_VALUE = 12;

        public Horse(Map map) :
            base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Fodder", "Grass", "SunflowerSeeds",
                "Banana", "BlueBerry", "Carrot", "Cabbage"};

        protected override string Dying()
        {
            return "Grunt grunt!";
        }

        public override string Feed(Food food)
        {
            if (this.HasGrown)
            {
                this.Diet.Add("Willow");
            }

            return base.Feed(food);
        }
    }
}