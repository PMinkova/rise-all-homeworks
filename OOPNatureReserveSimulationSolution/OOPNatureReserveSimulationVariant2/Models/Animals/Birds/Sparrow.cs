﻿using ReserveSimulation.Models.Foods;

namespace ReserveSimulation.Models.Animals.Birds
{
    using Map;

    public class Sparrow : Bird
    {
        private const int MAX_ENERGY = 70;
        private const int NUTRITIONAL_VALUE = 5;

        public Sparrow(Map map) :
            base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "SunflowerSeeds", "BlueBerry", "Grass" };

        protected override string Eating(Food food)
        {
            return "Chick-chi-rik, Chick-chi-rik, so delicious!";
        }
        public override string Feed(Food food)
        {
            if (this.HasGrown)
            {
                this.Diet.Add("Willow");
            }

            return base.Feed(food);
        }
    }
}