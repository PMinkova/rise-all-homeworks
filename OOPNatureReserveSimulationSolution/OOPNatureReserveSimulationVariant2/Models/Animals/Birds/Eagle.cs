﻿using ReserveSimulation.Models.Foods;

namespace ReserveSimulation.Models.Animals.Birds
{
    using Map;

    public class Eagle : Bird
    {
        private const int MAX_ENERGY = 100;
        private const int NUTRITIONAL_VALUE = 9;

        public Eagle(Map map) :
            base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; }
            = new HashSet<string> { "Deer", "Elk", "Horse", "Moose", "Rabbit", "Salmon", "Sparrow", "Lion", "Mouse" };

        protected override string Starving()
        {
            return "Give me some fresh meat or I will eat you!";
        }

        public override string Feed(Food food)
        {
            if (this.HasGrown)
            {
                this.Diet.Add("Sheep");
            }

            return base.Feed(food);
        }
    }
}