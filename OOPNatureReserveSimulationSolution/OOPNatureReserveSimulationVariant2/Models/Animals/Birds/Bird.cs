﻿namespace ReserveSimulation.Models.Animals.Birds
{
    using Map;

    public abstract class Bird : Animal
    {
        protected Bird(int maxEnergy, int nutritionalValue, Map map)
            : base(maxEnergy, nutritionalValue, map)
        {

        }
    }
}