﻿using ReserveSimulation.Models.Foods;

namespace ReserveSimulation.Models.Animals.Fishes
{
    using Map;

    public class Salmon : Fish
    {
        private const int MAX_ENERGY = 60;
        private const int NUTRITIONAL_VALUE = 10;

        public Salmon(Map map) 
            : base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Invertebrates", "Insects" };

        protected override string Dying()
        {
            return "Finally you can eat me!";
        }
        public override string Feed(Food food)
        {
            if (this.HasGrown)
            {
                this.Diet.Remove("Invertebrates");
                this.Diet.Remove("Insects");
                this.Diet.Add("Squid");
                this.Diet.Add("Krill");
            }

            return base.Feed(food);
        }
    }
}