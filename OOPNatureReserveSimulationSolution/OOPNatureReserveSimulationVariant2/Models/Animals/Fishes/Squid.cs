﻿namespace ReserveSimulation.Models.Animals.Fishes
{
    using Map;

    public class Squid : Fish
    {
        private const int MAX_ENERGY = 44;
        private const int NUTRITIONAL_VALUE = 7;

        public Squid(Map map) 
            : base(MAX_ENERGY, NUTRITIONAL_VALUE, map) 
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Algae" };
    }
}