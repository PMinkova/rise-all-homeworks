﻿namespace ReserveSimulation.Models.Animals.Fishes
{
    using Map;

    public class Invertebrates : Fish
    {
        private const int MAX_ENERGY = 90;
        private const int NUTRITIONAL_VALUE = 5;

        public Invertebrates(Map map) :
            base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Algae" };
    }
}