﻿using ReserveSimulation.Models.Foods;

namespace ReserveSimulation.Models.Animals.Fishes
{
    using Map;

    public abstract class Fish : Animal
    {
        protected Fish(int maxEnergy, int nutritionalValue, Map map)
            : base(maxEnergy, nutritionalValue, map)
        {

        }

        protected override string Eating(Food food)
        {
            return "Sorry, I can`t speak! Bulbuk, bulbuk";
        }
    }
}