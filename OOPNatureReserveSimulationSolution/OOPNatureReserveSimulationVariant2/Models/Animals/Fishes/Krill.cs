﻿namespace ReserveSimulation.Models.Animals.Fishes
{
    using Map;

    public class Krill : Fish
    {
        private const int MAX_ENERGY = 20;
        private const int NUTRITIONAL_VALUE = 5;

        public Krill(Map map) :
            base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Algae" };
    }
}