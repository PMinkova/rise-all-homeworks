﻿namespace ReserveSimulation.Models.Animals.Fishes
{
    using Map;

    public class Shark : Fish
    {
        private const int MAX_ENERGY = 120;
        private const int NUTRITIONAL_VALUE = 10;

        public Shark(Map map)
            : base(MAX_ENERGY, NUTRITIONAL_VALUE, map)
        {

        }

        protected override ICollection<string> Diet { get; } =
            new HashSet<string> { "Invertebrates", "Krill", "Salmon", "Squid" };
    }
}