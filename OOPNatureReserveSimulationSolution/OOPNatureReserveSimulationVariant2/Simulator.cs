﻿namespace ReserveSimulation
{
    using IO;

    using Models.Animals;
    using Models.Foods.Plants;
    using Models.Foods;
    using Models.Map;

    public class Simulator
    {
        private readonly IWriter writer;

        private readonly HashSet<Food> foods;
        private HashSet<Animal> animals;
        private HashSet<Plant> plants;
        private Tile tile;

        private Random random;

        public Simulator(IWriter writer, Random random, Tile tile)
        {
            this.tile = tile;
            this.foods = tile.Foods.ToHashSet();
            this.animals = tile.Animals.ToHashSet();
            this.plants = tile.Plants.ToHashSet();
            this.writer = writer;
            this.random = random;
        }

        public void SimulateLifeCycle(bool getDetailedStatistics)
        {
            var roundCounter = 1;

            while (animals.Any(a => a.IsAlive))
            {
                this.RegeneratePlants();

                this.OfferFoodToAnimal();

                if (getDetailedStatistics)
                {
                    this.GetCurrentRoundStatistics(roundCounter);
                }

                roundCounter++;
            }

            this.GetFinalStatistics();
        }

        private void OfferFoodToAnimal()
        {
            foreach (Animal animal in animals.Where(a => a.IsAlive))
            {
                var currentFood = this.GenerateRandomFood();

                writer.WriteLine(animal.Feed(currentFood));

                if (currentFood.NutritionalValue == 0 && !this.plants.Contains(currentFood)) 
                {
                    this.foods.Remove(currentFood);
                }

                if (animal.IsStarving)
                {
                    animal.TryToMove(this.tile, random);
                }
            }
        }

        private Food GenerateRandomFood()
        {
            var randomIndex = random.Next(0, foods.Count);
            var currentFood = foods.ElementAt(randomIndex);

            return currentFood;
        }

        private void GetCurrentRoundStatistics(int roundCounter)
        {
            var deadAnimals = animals.Count(a => !a.IsAlive);
            var aliveAnimals = animals.Count(a => a.IsAlive);

            writer.WriteLine($"Dead animals: {deadAnimals}, Alive animals: {aliveAnimals}");
            writer.WriteLine($"Alive animals on round {roundCounter}:");

            foreach (var animal in animals.Where(a => a.IsAlive))
            {
                writer.WriteLine($"{animal.GetType().Name}: {animal.LifeSpan} lifespan, {animal.CurrentEnergy} energy");
            }

            writer.WriteLine("");
        }

        private void GetFinalStatistics()
        {
            var animalWithMinLifespan = animals.OrderBy(a => a.LifeSpan).First();
            var minimumLifespan = animalWithMinLifespan.LifeSpan;
            var typeOfAnimalWithMinLifespan = animalWithMinLifespan.GetType().Name;

            var animalWithMaxLifespan = animals.OrderByDescending(a => a.LifeSpan).First();
            var maximumLifespan = animalWithMaxLifespan.LifeSpan;
            var typeOfAnimalWithMaxLifespan = animalWithMaxLifespan.GetType().Name;

            var averageLifeSpan = (int)animals.Average(x => x.LifeSpan);

            writer.WriteLine($"The minimum lifespan of an animal is {minimumLifespan} - {typeOfAnimalWithMinLifespan}.");
            writer.WriteLine($"The maximum lifespan of an animal is {maximumLifespan} - {typeOfAnimalWithMaxLifespan}.");
            writer.WriteLine($"The average lifespan of an animal is {averageLifeSpan}.");
        }

        private void RegeneratePlants()
        {
            foreach (Plant plant in plants.Where(p => !p.IsRegenerated))
            {
                plant.Regenerate();
            }
        }
    }
}