﻿namespace ReserveSimulation.IO
{
    public class FileWriter : IWriter
    {
        public FileWriter()
        {
            using StreamWriter writer = new StreamWriter("../../../output.txt", false);
            writer.WriteLine("");
        }

        public void WriteLine(object text)
        {
            using StreamWriter writer = new StreamWriter("../../../output.txt", true);
            writer.WriteLine(text);
        }
    }
}