﻿namespace ReserveSimulation.IO
{
    public class ConsoleWriter : IWriter
    {
        public void WriteLine(object text)
        {
            Console.WriteLine(text);
        }
    }
}
