﻿namespace ReserveSimulation.IO
{
    public interface IWriter
    {
        void WriteLine(object message);
    }
}
