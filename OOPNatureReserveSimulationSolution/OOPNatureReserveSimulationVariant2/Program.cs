﻿// See https://aka.ms/new-console-template for more information

using ReserveSimulation;
using ReserveSimulation.IO;

//IWriter fileWriter = new FileWriter();
IWriter consoleWriter = new ConsoleWriter();
var random = new Random();

Simulator simulator = new Simulator(consoleWriter, random);
simulator.SimulateLifeCycle(false);
