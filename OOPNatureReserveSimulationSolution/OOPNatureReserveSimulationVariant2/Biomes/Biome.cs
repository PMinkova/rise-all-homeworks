﻿namespace ReserveSimulation.Biomes
{
    using Models.Animals;
    using Models.Foods.Plants;
    using Models.Foods;
    using Models.Map;

    public abstract class Biome
    {
        private IEnumerable<Food> foods;

        protected abstract ICollection<Animal> CreateAnimals(Map map);

        protected abstract ICollection<Plant> CreatePlants();

        public Tile CreateTile(Map map)
        {
            return new Tile(CreateAnimals(map), CreatePlants());
        }
    }
}