﻿namespace ReserveSimulation.Biomes
{
    using Models.Animals;
    using Models.Animals.Birds;
    using Models.Animals.Mammals;
    using Models.Foods.Plants;
    using Models.Map;

    public class Forest : Biome
    {
        protected override ICollection<Animal> CreateAnimals(Map map)
        {
            return new List<Animal>
            {
                new Sparrow(map),
                new Eagle(map),
                new Deer(map),
                new Elk(map),
                new Horse(map),
                new Lion(map),
                new Mouse(map),
                new Rabbit(map),
                new Sheep(map)
            };
        }

        protected override ICollection<Plant> CreatePlants()
        {
            return new List<Plant>
            {
                new BlueBerry(),
                new Carrot(),
                new Cabbage(),
                new Fodder()
            };
        }
    }
}
