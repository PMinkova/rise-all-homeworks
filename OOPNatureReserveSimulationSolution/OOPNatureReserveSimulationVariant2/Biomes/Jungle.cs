﻿namespace ReserveSimulation.Biomes;

using Models.Animals;
using Models.Animals.Birds;
using Models.Animals.Mammals;
using Models.Animals.Others;
using Models.Foods.Plants;
using Models.Map;

public class Jungle : Biome
{
    protected override ICollection<Animal> CreateAnimals(Map map)
    {
        return new List<Animal>
        {
            new Lion(map),
            new Monkey(map),
            new Mouse(map),
            new Insects(map),
            new Eagle(map),
            new Sparrow(map)
        };
    }

    protected override ICollection<Plant> CreatePlants()
    {
        return new List<Plant>
        {
            new Banana(),
            new BlueBerry(),
            new Palm(),
        };
    }
}