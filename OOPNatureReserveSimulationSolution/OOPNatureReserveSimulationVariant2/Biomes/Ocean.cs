﻿namespace ReserveSimulation.Biomes
{
    using Models.Animals;
    using Models.Animals.Fishes;
    using Models.Foods.Plants;
    using Models.Map;

    public class Ocean : Biome
    {
        protected override ICollection<Animal> CreateAnimals(Map map)
        {
            return new List<Animal>
            {
                new Invertebrates(map),
                new Krill(map),
                new Salmon(map),
                new Shark(map),
                new Squid(map)
            };
        }

        protected override ICollection<Plant> CreatePlants()
        {
            return new List<Plant>
            {
                new Algae()
            };
        }
    }
}
