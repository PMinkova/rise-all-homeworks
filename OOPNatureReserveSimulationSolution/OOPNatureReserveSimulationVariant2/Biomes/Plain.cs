﻿namespace ReserveSimulation.Biomes
{
    using Models.Animals;
    using Models.Animals.Birds;
    using Models.Animals.Mammals;
    using Models.Foods.Plants;
    using Models.Map;

    public class Plain : Biome
    {
        protected override ICollection<Animal> CreateAnimals(Map map)
        {
            return new List<Animal>
            {
                new Sparrow(map),
                new Eagle(map),
                new Deer(map),
                new Horse(map),
                new Moose(map),
                new Rabbit(map),
                new Sheep(map)
            };
        }

        protected override ICollection<Plant> CreatePlants()
        {
            return new List<Plant>
            {
                new BlueBerry(),
                new Cabbage(),
                new Carrot(),
                new Fodder(),
            };
        }
    }
}
