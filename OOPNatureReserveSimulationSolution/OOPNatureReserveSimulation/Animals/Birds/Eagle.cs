﻿using OOPNatureReserveSimulation.Foods.Meat;

namespace OOPNatureReserveSimulation.Animals.Birds
{
    public class Eagle : Bird
    {
        private const int EagleMaxEnergy = 10;

        public Eagle() 
            : base(EagleMaxEnergy)
        {

        }

        protected override IReadOnlyCollection<Type> Diet
            => new HashSet<Type> {  typeof(ChickenMeat), typeof(FishMeat), typeof(PorkMeat) };
    }
}