﻿using OOPNatureReserveSimulation.Foods.Plants;

namespace OOPNatureReserveSimulation.Animals.Birds
{
    using Foods.Fruits;
    using Foods.Seeds;
    using System;
    using System.Collections.Generic;

    public class Sparrow : Bird
    {
        private const int SparrowMaxEnergy = 12;

        public Sparrow() 
            : base(SparrowMaxEnergy)
        {

        }

        protected override IReadOnlyCollection<Type> Diet =>
            new HashSet<Type> { typeof(SunflowerSeeds), typeof(BlueBerry), typeof(Grass) };
    }
}
