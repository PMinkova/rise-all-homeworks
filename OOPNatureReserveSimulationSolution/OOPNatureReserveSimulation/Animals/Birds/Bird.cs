﻿namespace OOPNatureReserveSimulation.Animals.Birds
{
    public abstract class Bird : Animal
    {
        protected Bird(int maxEnergy) 
            : base(maxEnergy)
        {

        }
    }
}
