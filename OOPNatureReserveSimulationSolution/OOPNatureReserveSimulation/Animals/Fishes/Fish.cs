﻿using OOPNatureReserveSimulation.Foods.Meat;

namespace OOPNatureReserveSimulation.Animals.Fishes
{
    public abstract class Fish : Animal
    {
        protected Fish(int maxEnergy) 
            : base(maxEnergy)
        {

        }

        protected override IReadOnlyCollection<Type> Diet =>
          new HashSet<Type> { typeof(FishMeat) };
    }
}
