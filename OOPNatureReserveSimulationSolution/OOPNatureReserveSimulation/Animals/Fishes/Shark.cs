﻿namespace OOPNatureReserveSimulation.Animals.Fishes
{
    public class Shark : Fish
    {
        private const int SharkMaxEnergy = 5;

        public Shark() 
            : base(SharkMaxEnergy)
        {

        }
    }
}
