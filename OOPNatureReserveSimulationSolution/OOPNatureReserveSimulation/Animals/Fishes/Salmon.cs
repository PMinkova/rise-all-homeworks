﻿namespace OOPNatureReserveSimulation.Animals.Fishes
{
    public class Salmon : Fish
    {
        private const int SalmonMaxEnergy = 10;

        public Salmon() 
            : base(SalmonMaxEnergy)
        {

        }
    }
}
