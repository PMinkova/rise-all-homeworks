﻿namespace OOPNatureReserveSimulation.Animals
{
    using Foods;

    public abstract class Animal
    {
        protected Animal(int maxEnergy)
        {
            this.CurrentEnergy = maxEnergy;
            this.MaxEnergy = maxEnergy;
            this.IsAlive = true;
        }

        public bool IsAlive { get; protected set; }

        public int LifeSpan { get; protected set; }

        protected abstract IReadOnlyCollection<Type> Diet { get; }

        protected int MaxEnergy { get; private set; }

        protected int CurrentEnergy { get; private set; }

        public void Feed(Food food)
        {
            if (this.IsAlive)
            {
                this.LifeSpan++;
            }

            if (this.Diet.Any(t => t.Name == food.GetType().Name))
            {
                this.CurrentEnergy++;

                if (this.CurrentEnergy > this.MaxEnergy)
                {
                    this.CurrentEnergy = this.MaxEnergy;
                }
            }
            else
            {
                this.CurrentEnergy--;

                if (this.CurrentEnergy <= 0)
                {
                    this.IsAlive = false;
                }
            }
        }
    }
}