﻿namespace OOPNatureReserveSimulation.Animals.Mammals
{
    public abstract class Mammal : Animal
    {
        protected Mammal(int maxEnergy) 
            : base(maxEnergy)
        {

        }
    }
}
