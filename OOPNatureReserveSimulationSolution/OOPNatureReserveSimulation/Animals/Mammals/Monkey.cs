﻿namespace OOPNatureReserveSimulation.Animals.Mammals
{
    using Foods.Fruits;
    using System;
    using System.Collections.Generic;

    public class Monkey : Mammal
    {
        private const int MonkeyMaxEnergy = 10;

        public Monkey() 
            : base(MonkeyMaxEnergy)
        {

        }

        protected override IReadOnlyCollection<Type> Diet =>
            new HashSet<Type> { typeof(Banana), typeof(BlueBerry) };
    }
}
