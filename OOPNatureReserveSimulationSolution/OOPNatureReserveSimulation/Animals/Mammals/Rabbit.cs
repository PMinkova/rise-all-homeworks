﻿using OOPNatureReserveSimulation.Foods.Plants;
using OOPNatureReserveSimulation.Foods.Vegetables;

namespace OOPNatureReserveSimulation.Animals.Mammals
{
    using System;
    using System.Collections.Generic;

    public class Rabbit : Mammal
    {
        private const int RabbitMaxEnergy = 6;

        public Rabbit() 
            : base(RabbitMaxEnergy)
        {

        }

        protected override IReadOnlyCollection<Type> Diet =>
            new HashSet<Type> { typeof(Carrot), typeof(Cabbage), typeof(Grass) };
    }
}
