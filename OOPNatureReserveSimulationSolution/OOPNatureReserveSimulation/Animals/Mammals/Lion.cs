﻿using OOPNatureReserveSimulation.Foods.Meat;

namespace OOPNatureReserveSimulation.Animals.Mammals
{
    using System;
    using System.Collections.Generic;

    public class Lion : Mammal
    {
        private const int LionMaxEnergy = 8;

        public Lion() 
            : base(LionMaxEnergy)
        {

        }

        protected override IReadOnlyCollection<Type> Diet =>
            new HashSet<Type> { typeof(ChickenMeat), typeof(PorkMeat) };
    }
}
