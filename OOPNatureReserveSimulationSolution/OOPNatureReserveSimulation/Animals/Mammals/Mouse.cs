﻿namespace OOPNatureReserveSimulation.Animals.Mammals
{
    using Foods.Fruits;
    using Foods.Seeds;
    using System;
    using System.Collections.Generic;

    public class Mouse : Mammal
    {
        private const int MouseMaxEnergy = 11;

        public Mouse() 
            : base(MouseMaxEnergy)
        {

        }

        protected override IReadOnlyCollection<Type> Diet =>
            new HashSet<Type> { typeof(SunflowerSeeds), typeof(Banana), typeof(BlueBerry) };
    }
}
