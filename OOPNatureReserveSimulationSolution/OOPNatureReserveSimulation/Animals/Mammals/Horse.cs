﻿using OOPNatureReserveSimulation.Foods.Plants;
using OOPNatureReserveSimulation.Foods.Vegetables;

namespace OOPNatureReserveSimulation.Animals.Mammals
{
    using Foods.Fruits;
    using Foods.Seeds;
    using System;
    using System.Collections.Generic;

    public class Horse : Mammal
    {
        private const int HorseMaxEnergy = 6;

        public Horse() 
            : base(HorseMaxEnergy)
        {

        }

        protected override IReadOnlyCollection<Type> Diet =>
            new HashSet<Type> { typeof(Fodder), typeof(Grass), typeof(SunflowerSeeds),
                typeof(Banana), typeof(BlueBerry), typeof(Carrot), typeof(Cabbage)};
    }
}
