﻿// See https://aka.ms/new-console-template for more information
using OOPNatureReserveSimulation;

var lifeSpanSimulator = new LifeSpanSimulator();
var statistics = lifeSpanSimulator.Run();

Console.WriteLine(statistics);
Console.Beep(40, 30000);