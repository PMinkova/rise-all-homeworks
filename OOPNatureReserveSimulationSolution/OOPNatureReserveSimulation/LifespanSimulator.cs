﻿using OOPNatureReserveSimulation.Foods.Meat;
using OOPNatureReserveSimulation.Foods.Plants;
using OOPNatureReserveSimulation.Foods.Vegetables;

namespace OOPNatureReserveSimulation
{
    using System.Text;

    using Animals;
    using Animals.Birds;
    using Animals.Fishes;
    using Animals.Mammals;
    using Foods;
    using Foods.Fruits;
    using Foods.Seeds;

    public class LifeSpanSimulator
    {
        private HashSet<Animal> animals;
        private HashSet<Food> foods;

        public LifeSpanSimulator()
        {
            animals = new HashSet<Animal>();
            foods = new HashSet<Food>();

            animals.Add(new Eagle());
            animals.Add(new Sparrow());
            animals.Add(new Salmon());
            animals.Add(new Shark());
            animals.Add(new Horse());
            animals.Add(new Lion());
            animals.Add(new Monkey());
            animals.Add(new Mouse());
            animals.Add(new Rabbit());

            foods.Add(new Banana());
            foods.Add(new BlueBerry());
            foods.Add(new FishMeat());
            foods.Add(new PorkMeat());
            foods.Add(new ChickenMeat());
            foods.Add(new Fodder());
            foods.Add(new Grass());
            foods.Add(new SunflowerSeeds());
            foods.Add(new Cabbage());
            foods.Add(new Carrot());
        }
        public string Run() 
        {
            Random random = new Random();

            while (animals.Any(a => a.IsAlive))
            {
                int randomIndex = random.Next(0, foods.Count - 1 );
                Food currentFood = foods.ElementAt(randomIndex);

                foreach (Animal animal in animals)
                {
                    animal.Feed(currentFood);
                }
            }

            Animal animalWithMinLifespan = animals.OrderBy(a => a.LifeSpan).First();
            int minimumLifespan = animalWithMinLifespan.LifeSpan;
            string typeOfanimalWithMinLifespan = animalWithMinLifespan.GetType().Name;

            Animal animalWithMaxLifespan = animals.OrderByDescending(a => a.LifeSpan).First();
            int maximumLifespan = animalWithMaxLifespan.LifeSpan;
            string typeOfanimalWithMaxLifespan = animalWithMaxLifespan.GetType().Name;

            int averageLifeSpan = (int)animals.Average(x => x.LifeSpan);

            StringBuilder statistics = new StringBuilder();

            statistics.AppendLine($"The minimum lifespan of an animal is {minimumLifespan} - {typeOfanimalWithMinLifespan}.");
            statistics.AppendLine($"The maximum lifespan of an animal is {maximumLifespan} - {typeOfanimalWithMaxLifespan}.");
            statistics.AppendLine($"The average lifespan of an animal is {averageLifeSpan}.");

            return statistics.ToString().TrimEnd();
        }
    }
}