namespace OOPNatureReserveSimulationVariant2Tests
{
    using ReserveSimulation.Models.Animals.Birds;
    using ReserveSimulation.Models.Animals.Fishes;
    using ReserveSimulation.Models.Animals.Mammals;

    [TestClass]
    public class AnimalTests
    {
        [TestMethod]
        public void ConstructorShouldSetIsAliveTrue()
        {
            var eagle = new Eagle();
            Assert.IsTrue(eagle.IsAlive);
        }

        [TestMethod]
        public void LifespanShouldIncreaseWithEveryFeedWhenAnimalIsAlive() 
        {
            var eagle = new Eagle();

            eagle.Feed(new Moose());
            eagle.Feed(new Rabbit());
            eagle.Feed(new Salmon());

            var expectedLifeSpan = 3;
            var actualLifeSpan = eagle.LifeSpan;

            Assert.AreEqual(expectedLifeSpan, actualLifeSpan);
        }

    //    [TestMethod]
    //    public void LifespanShouldNotIncreaseWhenAnimalIsDead()
    //    {
    //        var horse = new Horse();

    //        for (int i = 0; i < 20; i++)
    //        {
    //            horse.Feed(Food.ChickenMeat);
    //        }

    //        var expectedLifeSpan = 6;
    //        var actualLifeSpan = horse.LifeSpan;

    //        Assert.AreEqual(expectedLifeSpan, actualLifeSpan);
    //    }

    //    [TestMethod]
    //    public void FeedShouldKillAnimalWhenFeedingWithWrongFood() 
    //    {
    //        var horse = new Horse();

    //        horse.Feed(Food.PorkMeat);
    //        horse.Feed(Food.PorkMeat);
    //        horse.Feed(Food.PorkMeat);
    //        horse.Feed(Food.PorkMeat);
    //        horse.Feed(Food.PorkMeat);
    //        horse.Feed(Food.PorkMeat);
    //        horse.Feed(Food.PorkMeat);

    //        Assert.IsFalse(horse.IsAlive);
    //    }
    }
}