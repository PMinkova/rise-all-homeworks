﻿namespace OOPNatureReserveSimulationVariant2Tests
{
    using ReserveSimulation.Models.Animals.Birds;
    using ReserveSimulation.Models.Foods;
    using ReserveSimulation.Models.Foods.Seeds;

    public class TestSparrow : Sparrow
    {
        public TestSparrow() 
            : base() { }

        public int TestMaxEnergy => this.MaxEnergy;

        public new ICollection<string> Diet => base.Diet;

        public string TestEat(Food food) => base.Eating(food);
       
    }

    [TestClass]
    public class SparrowTests
    {
        [TestMethod]
        public void ConstructorShouldSetConstantsCorrectly()
        {
            var sparrow = new Sparrow();

            var expectedMaxEnergy = 70;
            var expectedNutritionalValue = 5;

            var actualMaxEnergy = new TestSparrow().TestMaxEnergy;
            var actualNutritionalValue = sparrow.NutritionalValue;

            Assert.AreEqual(expectedMaxEnergy, actualMaxEnergy);
            Assert.AreEqual(expectedNutritionalValue, actualNutritionalValue);
        }

        [TestMethod]
        public void DietShouldContainExpectedElements()
        {
            var sparrow = new TestSparrow();

            var expectedDiet = new List<string> { "SunflowerSeeds", "BlueBerry", "Grass" };
            var actualDiet = new List<string>(sparrow.Diet);

            CollectionAssert.AreEquivalent(expectedDiet, actualDiet);
        }

        [TestMethod]
        public void EatShouldReturnExpectedMessage()
        {
            var sparrow = new TestSparrow();
            var food = new SunflowerSeeds();

            var expectedMessage = "Chick-chi-rik, Chick-chi-rik, so delicious!";
            var actualMessage = sparrow.TestEat(food);

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public void FeedUpdatesDietWhenSparrowGrows()
        {
            var sparrow = new TestSparrow();
            var food = new SunflowerSeeds();

            for (int i = 0; i < 11; i++)
            {
                sparrow.Feed(food);
            }

            var expectedDiet = new List<string> { "SunflowerSeeds", "BlueBerry", "Grass", "Willow"};
            var actualDiet = new List<string>(sparrow.Diet);

            CollectionAssert.AreEqual(expectedDiet, actualDiet);
        }
    }
}