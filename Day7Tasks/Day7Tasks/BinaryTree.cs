﻿namespace Day7Tasks
{
    public class BinaryTree<T>
    {
        public BinaryTree(T value, BinaryTree<T> left, BinaryTree<T> right)
        {
            this.Value = value;
            this.Left = left;
            this.Right = right;
        }

        public T Value { get; set; }

        public BinaryTree<T> Left { get; set; }

        public BinaryTree<T> Right { get; set; }


        // 1.Implement PreOrder, PostOrder and InOrder traversals for a binary tree.

        public List<T> PreOrder()
        {
            var elements = new List<T>();

            elements.Add(this.Value);

            if (this.Left != null)
            {
                elements.AddRange(this.Left.PreOrder());
            }

            if (this.Right != null)
            {
                elements.AddRange(this.Right.PreOrder());
            }

            return elements;
        }

        public List<T> InOrder()
        {
            var elements = new List<T>();

            if (this.Left != null)
            {
                elements.AddRange(this.Left.InOrder());
            }

            elements.Add(this.Value);

            if (this.Right != null)
            {
                elements.AddRange(this.Right.InOrder());
            }

            return elements;
        }

        public List<T> PostOrder()
        {
            var elements = new List<T>();

            if (this.Left != null)
            {
                elements.AddRange(this.Left.PostOrder());
            }

            if (this.Right != null)
            {
                elements.AddRange(this.Right.PostOrder());
            }

            elements.Add(this.Value);

            return elements;
        }

        // 2. Implement algorithm to calculate height of a binary tree
        public static int GetTreeHeight(BinaryTree<T> node) 
        {
            if (node == null)
            {
                return 0;
            }

            var leftResult = GetTreeHeight(node.Left);
            var rightResult = GetTreeHeight(node.Right);

            if (leftResult > rightResult)
            {
                return leftResult + 1;
            }

            return rightResult + 1;
        }

        // 3.Check if a tree is a Binary Search Tree

        public bool IsBinarySearchTree()
        {
            var inOrderElements = InOrder().ToList();

            var listToSort = new List<T>(inOrderElements);
            listToSort.Sort();

            if (!Enumerable.SequenceEqual(inOrderElements, listToSort))
            {
                return false;
            }

            return true;
        }

        // 4.Kth Smallest Element in a BST

        public T GetKthSmallestElement(int position) 
        {
            var orderedElements = this.InOrder();

            if (position > orderedElements.Count)
            {
                throw new ArgumentException("No element at that position");
            }

            return orderedElements[position - 1];
        }
    }
}
