﻿namespace Day7Tasks
{
    public class FifthTask
    {
        public static int[] GetOrderedElements(int[,] matrix) 
        {
            var priorityElements = new PriorityQueue<int, int>();

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    var currentNumber = matrix[i, j];

                    priorityElements.Enqueue(currentNumber, currentNumber);
                }
            }

            var orderedNumbers = new List<int>();

            while (priorityElements.Count > 0)
            {
                orderedNumbers.Add(priorityElements.Dequeue());
            }
            
            return orderedNumbers.ToArray();
        }
    }
}
