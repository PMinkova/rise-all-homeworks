﻿namespace Day7TasksTests
{
    using Day7Tasks;

    [TestClass]
    public class SixthTaskTests
    {
        [TestMethod]
        public void HeapSortShouldReturnSortedArray()
        {
            var numbers = new int[] { 20, 40, 100, 60, 50, 10, 30 };

            var expected = new int[] { 10, 20, 30, 40, 50, 60, 100 };
            var actual = SixthTask.HeapSort(numbers);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void HeapSortShouldThrowSorEmptyArray()
        {
            var numbers = new int[] { };

            Assert.ThrowsException<ArgumentException>(() => SixthTask.HeapSort(numbers), "The length of the array should be at least 1");
        }
    }
}
