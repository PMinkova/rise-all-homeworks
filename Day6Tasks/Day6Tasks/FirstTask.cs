﻿namespace Day6Tasks
{
    using System.Collections;
    using System;
    using System.Text;

    public class FirstTask
    {
        /* Task 1: Follow the example from the presentation. Create a similar hash table or dictionary. Fill it 
        with 5-10 cars and owners.
        Access the name of the owner of the car with the coolest plate number.
        If there are owners that own more than one car print their names on the console.*/

        public static string GetCoolestNumberOwner(Dictionary<string, string> carsNumbers)
        {
            if (carsNumbers.ContainsKey("XAXAXAXA"))
            {
                return carsNumbers["XAXAXAXA"];
            }

            throw new ArgumentException("The coolest number is not present in the list");
        }

        public static List<string> GetOwnersWithMoreThenOneCar(Dictionary<string, string> carsNumbers)
        {
            var owners = new Dictionary<string, List<string>>();

            foreach (var (carNumber, name) in carsNumbers)
            {
                if (!owners.ContainsKey(name))
                {
                    owners.Add(name, new List<string>());
                }

                owners[name].Add(carNumber);
            }

            var result = new List<string>();

            foreach (var (name, cars) in owners)
            {
                if (cars.Count > 1)
                {
                    result.Add(name);
                }
            }

            return result;
        }
    }
}
