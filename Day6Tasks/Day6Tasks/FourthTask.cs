﻿namespace Day6Tasks
{
    public class FourthTask
    {
        /*Task 4: Implement a spell checker that takes a dictionary of words and a document/string, and 
         returns all misspelled words in the document/string.
         Hint: HashSet*/

        public static List<string> GetAllMisspelledWords(HashSet<string> words, string document) 
        {
            var textArray = document.Split(" ", StringSplitOptions.RemoveEmptyEntries).ToArray();
            var result = new List<string>();

            foreach (var word in textArray)
            {
                if (!words.Contains(word))
                {
                    result.Add(word);
                }
            }

            return result;
        }
    }
}
