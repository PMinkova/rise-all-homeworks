﻿namespace Day6Tasks
{
    using System.Security.Cryptography;
    using System.Text;

    public class SixthTask
    {
        /*Task 6:Assume you are implementing a web application 
         * that will require a user log-in. Write a function that 
         * creates new user account, the function should take the 
         * user’s name and the user password as arguments and will 
         * store them in a suitable data structure, however the password 
         * must not be stored as plain text–use the SHA256 algorithm instead.
         * Hint: There is no need to implement the algorithm yourself, 
         * C# has it internally –just google how to use it.*/

        public static Dictionary<string, string> CreateNewUserAccount(string username, string password) 
        {
            var encriptedPassword = ComputeSHA256(password);

            var dictionary = new Dictionary<string, string>();
            dictionary.Add(encriptedPassword, username);

            return dictionary;
        }

        private static string ComputeSHA256(string password) 
        {
            string hash = String.Empty;

            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] hashValue = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));

                foreach (byte b in hashValue)
                {
                    hash += $"{b:X2}";
                }
            }

            return hash;
        }
    }
}
