﻿namespace Day6Tasks
{
    using System;
    using System.Collections.Generic;

    public class CustomHashTable<T>
    {
        private LinkedList<KeyValuePair<string, T>>[] elements;
        private int count;

        public CustomHashTable()
        {
            this.elements = new LinkedList<KeyValuePair<string, T>>[10];
            for (int i = 0; i < this.elements.Length; i++)
            {
                this.elements[i] = new LinkedList<KeyValuePair<string, T>>();
            }
        }

        public int GetCount()
        {
            return this.count;
        }

        public void Insert(string key, T value)
        {
            var indexToInsert = this.GetArrayPosition(key);

            this.elements[indexToInsert].AddFirst(new KeyValuePair<string, T>(key, value));
            this.count++;
        }

        public bool Contains(string key)
        {
            var index = this.GetArrayPosition(key);

            foreach (var item in this.elements[index])
            {
                if (item.Key == key)
                {
                    return true;
                }
            }

            return false;
        }

        public void Delete(string key)
        {
            if (!this.Contains(key))
            {
                throw new ArgumentException("The element does not exist");
            }

            var index = this.GetArrayPosition(key);

            foreach (var item in this.elements[index])
            {
                if (item.Key == key)
                {
                    this.elements[index].Remove(item);
                    this.count--;
                    break;
                }
            }
        }

        private int GetArrayPosition(string key)
        {
            var position = 0;

            for (int i = 0; i < key.Length; i++)
            {
                position += key[i];
            }

            return position % 10;
        }
    }
}
