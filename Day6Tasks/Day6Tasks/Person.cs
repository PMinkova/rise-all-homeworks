﻿namespace Day6Tasks
{
    using System.Text.RegularExpressions;

    public class Person
    {
        /*Task 7: Implement a hash function for a custom data structure that contains a first name, last name, and age. 
         * The hash function should return a unique hash code based on these fields.*/
        public Person(string firstName, string lastName, int age)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Age = age;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public override bool Equals(object? obj)
        {
            var other = obj as Person;

            if (this.FirstName == other.FirstName && this.LastName == other.LastName && this.Age == other.Age)
            {
                return true;
            }

            return false;
        }
        public override int GetHashCode()
        {
            var firstNameGetHashCode = 0;

            for (int i = 0; i < this.FirstName.Length; i++)
            {
                firstNameGetHashCode += this.FirstName[i];
            }

            var lastNameGetHashCode = 0;

            for (int i = 0; i < this.FirstName.Length; i++)
            {
                lastNameGetHashCode += this.FirstName[i];
            }

            return firstNameGetHashCode + lastNameGetHashCode * this.Age;
        }
    }
}
