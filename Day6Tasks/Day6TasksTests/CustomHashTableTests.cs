﻿namespace Day6TasksTests
{
    using Day6Tasks;

    [TestClass]
    public class CustomHashTableTests
    { 
        [TestMethod]
        public void CustomHashTableShoulAddElementsProperly() 
        {
            var names = new CustomHashTable();

            var name = "Petya";
            var secondName = "Nadya";

            names.Insert(name);
            names.Insert(secondName);

            Assert.AreEqual(2, names.GetCount());
        }

        [TestMethod]
        public void ContainsReturnsFalseForNonexistentElement()
        {
            var hashTable = new CustomHashTable();

            var result = hashTable.Contains("Stela");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void InsertIncrementsCount()
        {
            var hashTable = new CustomHashTable();

            hashTable.Insert("Tanya");

            Assert.AreEqual(1, hashTable.GetCount());
        }

        [TestMethod]
        public void DeleteRemovesElementFromHashTable()
        {
            var hashTable = new CustomHashTable();

            hashTable.Insert("Petya");
            hashTable.Delete("Petya");

            Assert.IsFalse(hashTable.Contains("Petya"));
        }

        [TestMethod]
        public void DeleteShouldwThrowForNonExistingElement()
        {
            var hashTable = new CustomHashTable();

            hashTable.Insert("Petya");

            var exceptionMessage = "The element does not exist";
            Assert.ThrowsException<ArgumentException>(() => hashTable.Delete("Stela"), exceptionMessage);
        }
    }
}
