﻿namespace Day6TasksTests
{
    using Day6Tasks;

    [TestClass]
    public class SixthTaskTests
    {
        [TestMethod]
        public void CreateNewUserAccountShouldWorkProperly() 
        {
            var username = "Petya";
            var password = "alabala123";

            var expectedPassword = "24FC5A9808BB232F365C640B96AF326A3E6413EB0482609B06600006EB3CC36F";
            
            var resultDictionary = SixthTask.CreateNewUserAccount(username, password);
            var actualPassword = resultDictionary.Keys.First();

            Assert.AreEqual(expectedPassword, actualPassword);
        }
    }
}
