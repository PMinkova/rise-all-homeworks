namespace FileSystemTaskTests
{
    using FileSystemTask;
    using System.Text;

    [TestClass]
    public class FileTests
    {

        [TestMethod]
        public void ConstructorSetsNameProperly()
        {
            var name = "test.txt";
            var file = new File(name);

            Assert.AreEqual(name, file.Name);
        }

        [TestMethod]
        public void ConstructorThrowsExceptionWhenNameIsNull()
        {
            string name = null;
            var exceptionMessage = $"{nameof(File.Name)} should contain at least 2 letters and a dot.";

            Assert.ThrowsException<ArgumentException>(() => new File(name), exceptionMessage);
        }

        [TestMethod]
        public void ConstructorThrowsExceptionWhenNameIsTooShort()
        {
            string name = "t.";
            var exceptionMessage = $"{nameof(File.Name)} should contain at least 2 letters and a dot.";

            Assert.ThrowsException<ArgumentException>(() => new File(name), exceptionMessage);
        }

        [TestMethod]
        public void ConstructorThrowsExceptionWhenNameIsEmptyString()
        {
            string name = "";
            var exceptionMessage = $"{nameof(File.Name)} should contain at least 2 letters and a dot.";

            Assert.ThrowsException<ArgumentException>(() => new File(name), exceptionMessage);
        }

        [TestMethod]
        public void ConstructorThrowsExceptionWhenNameIsWhiteSpace()
        {
            string name = " ";
            var exceptionMessage = $"{nameof(File.Name)} should contain at least 2 letters and a dot.";

            Assert.ThrowsException<ArgumentException>(() => new File(name), exceptionMessage);
        }

        [TestMethod]
        public void ConstructorThrowsExceptionWhenNameDoesNotContainDot()
        {
            string name = "test";
            var exceptionMessage = $"{nameof(File.Name)} should contain at least 2 letters and a dot.";

            Assert.ThrowsException<ArgumentException>(() => new File(name), exceptionMessage);
        }

        [TestMethod]
        public void SizeReturnsCorrectSize()
        {
            var file = new File("test.txt");

            file.AddContent(1, "hi");
            file.AddContent(5, "there");

            Assert.AreEqual(12, file.Size);
        }

        [TestMethod]
        public void SizeReturnsZeroForEmptyFile()
        {
            var file = new File("test.txt");
            Assert.AreEqual(0, file.Size);
        }

        [TestMethod]
        public void LinesCountReturnsCorrectCount()
        {
            var file = new File("test.txt");

            file.AddContent(8, "hi");
            file.AddContent(2, "there");

            Assert.AreEqual(8, file.LinesCount);
        }

        [TestMethod]
        public void LinesCountReturnsZeroForEmptyFile()
        {
            var file = new File("test.txt");
            Assert.AreEqual(0, file.LinesCount);
        }

        [TestMethod]
        public void WordsCountReturnsCorrectCount()
        {
            var file = new File("test.txt");

            file.AddContent(5, "Hi there!");
            file.AddContent(1, "My name is Petya!");

            Assert.AreEqual(6, file.WordsCount);
        }

        [TestMethod]
        public void WordsCountReturnsZeroForEmptyFile()
        {
            var file = new File("test.txt");
            Assert.AreEqual(0, file.WordsCount);
        }

        [TestMethod]
        public void AddContentThrowsExceptionWhenLineIsNegative()
        {
            var file = new File("test.txt");
            var line = -1;
            var content = "Hi there!";
            var exceptionMessage = "Line cannot be zero or negative.";

            Assert.ThrowsException<ArgumentException>(() => file.AddContent(line, content), exceptionMessage);
        }

        [TestMethod]
        public void AddContentThrowsExceptionWhenLineIsZero()
        {
            var file = new File("test.txt");
            var line = 0;
            var content = "Hi there!";
            var exceptionMessage = "Line cannot be zero or negative.";

            Assert.ThrowsException<ArgumentException>(() => file.AddContent(line, content), exceptionMessage);
        }

        [TestMethod]
        public void AddContentThrowsExceptionWhenContentIsNull()
        {
            var file = new File("test.txt");
            var line = 10;   
            string content = null;
            var exceptionMessage = "Line cannot be zero or negative.";

            Assert.ThrowsException<ArgumentException>(() => file.AddContent(line, content), exceptionMessage);
        }

        [TestMethod]
        public void AddContentThrowsExceptionWhenContentIsEmpty()
        {
            var file = new File("test.txt");
            var line = 10;
            var content = string.Empty;
            var exceptionMessage = "Line cannot be zero or negative.";

            Assert.ThrowsException<ArgumentException>(() => file.AddContent(line, content), exceptionMessage);
        }

        [TestMethod]
        public void AddContentThrowsExceptionWhenContentIsWhiteSpace()
        {
            var file = new File("test.txt");
            var line = 10;
            var content = " ";
            var exceptionMessage = "Line cannot be zero or negative.";

            Assert.ThrowsException<ArgumentException>(() => file.AddContent(line, content), exceptionMessage);
        }

        [TestMethod]
        public void GetFileContentReturnsCorrectContent()
        {
            var file = new File("test.txt");

            file.AddContent(2, "Hi");
            file.AddContent(4, "there!");

            var expected = new StringBuilder();

            expected.AppendLine();
            expected.AppendLine("Hi");
            expected.AppendLine();
            expected.AppendLine("there!");

            Assert.AreEqual(expected.ToString(), file.GetFileContent());
        }

        [TestMethod]
        public void GetFileContentReturnsEmptyStringForEmptyFile()
        {
            var file = new File("test.txt");
            Assert.AreEqual(string.Empty, file.GetFileContent());
        }

        [TestMethod]
        public void GetLastLinesReturnCorrectContent() 
        {
            var file = new File("test.txt");

            file.AddContent(2, "Hi");
            file.AddContent(4, "there!");
            file.AddContent(7, "How are you?");

            var count = 5;
            var expected = new StringBuilder();

            expected.AppendLine();
            expected.AppendLine("there!");
            expected.AppendLine();
            expected.AppendLine();
            expected.AppendLine("How are you?");

            Assert.AreEqual(expected.ToString(), file.GetLastLines(count));
        }

        [TestMethod]
        public void GetLastLinesReturnCorrectContentForOnePopulatedLine()
        {
            var file = new File("test.txt");

            file.AddContent(5, "Hi");

            var count = 3;
            var expected = new StringBuilder();

            expected.AppendLine();
            expected.AppendLine();
            expected.AppendLine("Hi");

            Assert.AreEqual(expected.ToString(), file.GetLastLines(count));
        }
    }
}