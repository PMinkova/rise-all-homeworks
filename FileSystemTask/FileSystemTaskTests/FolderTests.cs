﻿namespace FileSystemTaskTests
{
    using FileSystemTask;
    using System.Runtime.InteropServices;
    using System.Text;

    [TestClass]
    public class FolderTests
    {

        [TestMethod]
        public void ConstructorSetsNameProperly()
        {
            var name = "Home";
            var folder = new Folder(name, null);

            Assert.AreEqual(name, folder.Name);
        }

        [TestMethod]
        public void ConstructorThrowsExceptionWhenNameIsNull()
        {
            string name = null;
            var exceptionMessage = $"{nameof(Folder.Name)} should contain at least 2 letters.";

            Assert.ThrowsException<ArgumentException>(() => new Folder(name, null), exceptionMessage);
        }

        [TestMethod]
        public void ConstructorThrowsExceptionWhenNameIsWhiteSpace()
        {
            string name = " ";
            var exceptionMessage = $"{nameof(Folder.Name)} should contain at least 2 letters.";

            Assert.ThrowsException<ArgumentException>(() => new Folder(name, null), exceptionMessage);
        }

        [TestMethod]
        public void ConstructorThrowsExceptionWhenNameIsEmptyString()
        {
            string name = string.Empty;
            var exceptionMessage = $"{nameof(Folder.Name)} should contain at least 2 letters.";

            Assert.ThrowsException<ArgumentException>(() => new Folder(name, null), exceptionMessage);
        }

        [TestMethod]
        public void ConstructorThrowsExceptionWhenNameIsOneLetter()
        {
            string name = "f";
            var exceptionMessage = $"{nameof(Folder.Name)} should contain at least 2 letters.";

            Assert.ThrowsException<ArgumentException>(() => new Folder(name, null), exceptionMessage);
        }

        [TestMethod]
        public void ConstructorSetsParentFolderProperly() 
        {
            var name = "Rise";
            var parentFolder = new Folder("Home", null);

            var folder = new Folder(name, parentFolder);

            Assert.AreEqual(parentFolder, folder.ParentFolder);
        }

        [TestMethod]
        public void CreateFileAddsFileToFolder()
        {
            var folder = new Folder("Home", null);
            var expectedFile = new File("rise.txt");

            folder.CreateFile("rise.txt");

            Assert.IsTrue(folder.GetAllFilesNames().Contains(expectedFile.Name));
        }

        [TestMethod]
        public void CreateFolderShouldAddNewFolderToAllFilesAndFolders()
        {
            var folderName = "Rise";
            var parentFolder = new Folder("Home", null);
            var expectedFolder = new Folder(folderName, parentFolder);

            parentFolder.CreateFolder(folderName);

            var actualFolder = parentFolder.GetFolder(folderName);
            Assert.AreEqual(expectedFolder.Name, actualFolder.Name);
        }

        [TestMethod]
        public void GetFileReturnsFileWithGivenName()
        {
            var folder = new Folder("Home", null);
            var expectedFile = new File("rise.txt");
            folder.CreateFile("rise.txt");

            var result = folder.GetFile("rise.txt");

            Assert.AreEqual(expectedFile.Name, result.Name);
        }

        [TestMethod]
        public void GetFileThrowsExceptionWhenFileNotFound()
        {
            var folder = new Folder("Home", null);
            var expectedFile = new File("rise.txt");

            var newFileName = "test.txt";
            folder.CreateFile(newFileName);

            var exceptionMessage = $"{newFileName} does not exist.";

            Assert.ThrowsException<ArgumentException>(() => folder.GetFile("rise.txt"), exceptionMessage);
        }

        [TestMethod]
        public void GetFolderReturnsFolderWithGivenName() 
        {
            var folderName = "Rise";
            var parentFolder = new Folder("Home", null);
            var expectedFolder = new Folder(folderName, parentFolder);

            parentFolder.CreateFolder(folderName);

            var actualFolder = parentFolder.GetFolder(folderName);
            Assert.AreEqual(expectedFolder.Name, actualFolder.Name);
        }

        [TestMethod]
        public void GetFolderThrowsExceptionWhenFolderNotFound()
        {
            var currentFolder = new Folder("Home", null);
            var searchedFolderName = "Rise";

            var expectedMessage = $"{searchedFolderName} does not exist.";

            Assert.ThrowsException<ArgumentException>(() => currentFolder.GetFolder(searchedFolderName), expectedMessage);
        }

        [TestMethod]
        public void GetAllFilesNamesReturnsListOfFilesAndFolderNames()
        {
            var folder = new Folder("Home", null);

            folder.CreateFile("rise1.txt");
            folder.CreateFile("rise2.txt");
            folder.CreateFile("rise3.txt");
            folder.CreateFolder("Rise");

            var expected = new StringBuilder();

            expected.AppendLine("rise1.txt");
            expected.AppendLine("rise2.txt");
            expected.AppendLine("rise3.txt");
            expected.Append("Rise");

            var actual = folder.GetAllFilesNames();

            Assert.AreEqual(expected.ToString(), actual);
        }

        [TestMethod]
        public void GetAllFilesNamesSortedDescendingReturnsFileNamesInDescendingOrder() 
        {
            var folder = new Folder("Home", null);

            folder.CreateFile("rise1.txt");
            folder.CreateFile("rise2.txt");
            folder.CreateFolder("Homework");

            folder.GetFile("rise1.txt").AddContent(1, "Hello");
            folder.GetFile("rise2.txt").AddContent(5, "Hello there");

            var expected = new StringBuilder();

            expected.AppendLine("rise2.txt");
            expected.AppendLine("rise1.txt");
            expected.Append("Homework");

            var actual = folder.GetAllFilesNamesSortedDescending();

            Assert.AreEqual(expected.ToString(), actual);
        }

        [TestMethod]
        public void SizeReturnsSumOfAllFilesAndFolders() 
        {
            var folder = new Folder("Home", null);

            folder.CreateFile("rise1.txt");
            folder.CreateFile("rise2.txt");

            folder.GetFile("rise1.txt").AddContent(2, "Hello"); // 5 symbols + 2 lines = 7
            folder.GetFile("rise2.txt").AddContent(5, "Hi"); // 2 symbols + 5 lines = 7

            var expectedSize = 14;
            var actual = folder.Size;

            Assert.AreEqual(expectedSize, actual);
        }
    }
}
