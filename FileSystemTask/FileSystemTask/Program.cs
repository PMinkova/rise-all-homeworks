﻿// See https://aka.ms/new-console-template for more information
using FileSystemTask;
using System.Runtime.Intrinsics.X86;

var fileSystem = new FileSystem();

while (true)
{
    var input = Console.ReadLine();

    if (input == "exit")
    {
        break;
    }

    try
    {
        if (input == "cd") // Does cd goes to Home directory?
        {
            fileSystem.GoToFolder("Home");
            Console.WriteLine($"Current folder: {fileSystem.CurrentFolder?.Name}");
        }
        else if (input == "cd .")
        {
            Console.WriteLine($"Current folder: {fileSystem.CurrentFolder?.Name}");
        }
        else if (input == "cd ..")
        {
            fileSystem.GoToPreviousDirectory();
            Console.WriteLine($"Current folder: {fileSystem.CurrentFolder?.Name}");
        }
        else if (input!.StartsWith("cd")) // cd location
        {
            var folderName = input.Substring(3);  // the folder name starts from index 3

            fileSystem.GoToFolder(folderName);

            if (folderName != "Home" && fileSystem.CurrentFolder?.Name == "Home")
            {
                Console.WriteLine("No such a folder");
            }
            else
            {
                Console.WriteLine($"Current folder: {fileSystem.CurrentFolder?.Name}");
            }
        }
        else if (input.StartsWith("mkdir"))  // mkdir < dir_name > folderName starts from index 6
        {
            var folderName = input.Substring(6);
            fileSystem.CurrentFolder?.CreateFolder(folderName);
            Console.WriteLine($"Directory {folderName} has been successfully created.");
        }
        else if (input.StartsWith("create_file")) //create_file < file_name > fileName Starts from index 12
        {
            var fileName = input.Substring(12);
            fileSystem.CurrentFolder?.CreateFile(fileName);
            Console.WriteLine($"{fileName} has been successfully created.");
        }
        else if (input.StartsWith("cat")) // cat < file_name > fileName starts from index 4
        {
            var fileName = input.Substring(4);
            var file = fileSystem.CurrentFolder?.GetFile(fileName);

            Console.WriteLine("File content:");
            Console.WriteLine(file?.GetFileContent());
        }
        else if (input.StartsWith("tail"))  // tail < file_name > fileName starts from index 5
        {
            var linesCount = 10;
            var fileName = input.Substring(5);

            if (input.Contains("-l"))
            {
                linesCount = int.Parse(input.Last().ToString());
                fileName = input.Split(" ", StringSplitOptions.RemoveEmptyEntries).Last();
            }

            var file = fileSystem.CurrentFolder?.GetFile(fileName);

            Console.WriteLine($"Last {linesCount}:");
            Console.WriteLine(file?.GetLastLines(linesCount));
        }
        else if (input.StartsWith("write"))
        {
            var splitedInput = input.Split(" ", StringSplitOptions.RemoveEmptyEntries);

            var fileName = splitedInput[1];
            var lineNumber = int.Parse(splitedInput[2]);

            var startIndex = fileName.Length + lineNumber.ToString().Length + "write".Length + 3;
            var fileContent = input.Substring(startIndex);

            var file = fileSystem.CurrentFolder?.GetFile(fileName);

            file?.AddContent(lineNumber, fileContent);
            Console.WriteLine($"Content has been successfully added to file {fileName}");
        }
        else if (input == "ls")
        {
            Console.WriteLine($"Files located in current directory {fileSystem.CurrentFolder}:");
            Console.WriteLine(fileSystem.CurrentFolder?.GetAllFilesNames());
        }
        else if (input == "ls --sorted desc")
        {
            Console.WriteLine($"Files located in current directory {fileSystem.CurrentFolder}, sorted by size descending:");
            Console.WriteLine(fileSystem.CurrentFolder?.GetAllFilesNamesSortedDescending());
        }
        else if (input.StartsWith("wc -l"))
        {
            var inputArguments = input.Substring(6);

            if (Path.HasExtension(inputArguments))
            {
                var file = fileSystem.CurrentFolder?.GetFile(inputArguments);
                Console.Write("The number of lines in the current file is: ");
                Console.WriteLine(file.LinesCount);
            }
            else
            {
                inputArguments += Environment.NewLine;

                while ((input = Console.ReadLine()) != "")
                {
                    inputArguments += input + Environment.NewLine;

                    ConsoleKeyInfo keyInfo = Console.ReadKey(intercept: true);

                    if (keyInfo.Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }

                inputArguments.TrimEnd();
                Console.Write("The number of lines in the current text is: ");
                Console.WriteLine(fileSystem.GetLinesCountInText(inputArguments));
            }
        }
        else if (input.StartsWith("wc"))
        {
            var inputArguments = input.Substring(3);

            if (Path.HasExtension(inputArguments))
            {
                var file = fileSystem.CurrentFolder?.GetFile(inputArguments);
                Console.WriteLine(file.WordsCount);
            }
            else
            {
                inputArguments += Environment.NewLine;

                while ((input = Console.ReadLine()) != "")
                {
                    inputArguments += input + Environment.NewLine;

                    ConsoleKeyInfo keyInfo = Console.ReadKey(intercept: true);

                    if (keyInfo.Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }

                inputArguments.TrimEnd();
                Console.Write("The number of words in the current text is: ");
                Console.WriteLine(fileSystem.GetWordsCountInText(inputArguments));
            }
        }
        else
        {
            Console.WriteLine($"'{input}' is not recognized as a valid command");
        }
    }
    catch (ArgumentException ex)
    {
        Console.WriteLine(ex.Message);
    }
}