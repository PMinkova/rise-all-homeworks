﻿namespace FileSystemTask
{
    using System.Text;

    public class Folder : ISizable
    {
        private string name;
        private HashSet<ISizable> allFilesAndFolders;

        public Folder(string name, Folder? parentFolder)
        {
            this.Name = name;
            this.ParentFolder = parentFolder;
            this.allFilesAndFolders = new HashSet<ISizable>();
        }

        public string Name
        {
            get 
            {
                return this.name;
            }
            private set 
            {
                if (string.IsNullOrEmpty(value) || value.Length < 2)
                {
                    throw new ArgumentException($"{nameof(Name)} should contain at least 2 letters.");
                }

                this.name = value;
            }
        }

        public int Size => this.allFilesAndFolders.Sum(f => f.Size);

        public Folder? ParentFolder { get; set; }

        public void CreateFile(string name)  //create_file < file_name > 
        {
            var newFile = new File(name);
            this.allFilesAndFolders.Add(newFile);
        }

        public void CreateFolder(string name) // mkdir < dir_name >
        {
            var newFolder = new Folder(name, this);
            allFilesAndFolders.Add(newFolder);
        }

        public File GetFile(string name)
        {
            var file = (File?)this.allFilesAndFolders.FirstOrDefault(f => f.Name == name);

            if (file == null)
            {
                throw new ArgumentException($"{name} does not exist.");
            }

            return file;
        }

        public Folder GetFolder(string name) 
        {
            var folder = (Folder?)this.allFilesAndFolders.FirstOrDefault(f => f.Name == name);

            if (folder == null)
            {
                throw new ArgumentException($"{name} does not exist.");
            }

            return folder;
        }

        public string GetAllFilesNames()
        {
            var fileNames = new StringBuilder();

            foreach (ISizable file in this.allFilesAndFolders)
            {
                fileNames.AppendLine(file.Name);
            }

            return fileNames.ToString().TrimEnd();
        }

        public string GetAllFilesNamesSortedDescending()
        {
            var sortedFiles = new StringBuilder();

            foreach (ISizable file in this.allFilesAndFolders.OrderByDescending(f => f.Size))
            {
                sortedFiles.AppendLine(file.Name);
            }

            return sortedFiles.ToString().TrimEnd();
        }
    }
}
