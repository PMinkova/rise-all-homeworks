﻿namespace FileSystemTask
{
    using System.Text;

    public class File : ISizable
    {
        private string name;
        private SortedDictionary<int, string> content;

        public File(string name)
        {
            this.content = new SortedDictionary<int, string>();
            this.Name = name;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            private set
            {
                if (string.IsNullOrEmpty(value) || value.Length < 3 || !value.Contains('.'))
                {
                    throw new ArgumentException($"{nameof(Name)} should contain at least 2 letters and a dot.");
                }

                this.name = value;
            }
        }

        public int Size => this.content.Keys.Any() ? this.content.Values.Sum(key => key.Length) + this.LinesCount : 0;

        public int LinesCount => this.content.Keys.Any() ? this.content.Keys.Last() : 0;

        public int WordsCount => this.content.Values.Any() ? this.content.Values.Sum(v => v.Split(' ', StringSplitOptions.RemoveEmptyEntries).Length) : 0;

        public void AddContent(int line, string content) //write < file_name > < line_number > < line_content >
        {
            if (line <= 0)
            {
                throw new ArgumentException("Line cannot be zero or negative.");
            }

            if (string.IsNullOrWhiteSpace(content))
            {
                throw new ArgumentException("Content should contains at least 1 symbol.");
            }

            if (!this.content.ContainsKey(line))
            {
                this.content.Add(line, content);
            }
            else
            {
                this.content[line] = content;
            }
        }

        public string GetFileContent() // cat < file_name > - displays the file contents
        {
            var startLine = 1;
            var lastLine = this.content.Keys.Any() ? this.content.Keys.Last() : 0;

            return this.GetContent(startLine, lastLine);
        }

        public string GetLastLines(int count)  // tail < file_name >
        {
            var startLine = this.content.Keys.Any() ? this.content.Keys.Last() - count + 1: 0;
            var endLine = this.content.Keys.Any() ? this.content.Keys.Last() : 0;

            return this.GetContent(startLine, endLine);
        }

        private string GetContent(int startLine, int endLine)
        {
            var content = new StringBuilder();

            for (int i = startLine; i <= endLine; i++)
            {
                if (this.content.ContainsKey(i))
                {
                    content.AppendLine(this.content[i]);
                }
                else
                {
                    content.AppendLine();
                }
            }

            return content.ToString();
        }
    }
}

