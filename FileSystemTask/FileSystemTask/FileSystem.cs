﻿namespace FileSystemTask
{
    public class FileSystem
    {
        private Folder home;

        public FileSystem()
        {
            this.home = new Folder("Home", null);
            this.CurrentFolder = home;
        }

        public Folder CurrentFolder { get; private set; }

        public void GoToFolder(string name)
        {
            if (name == "Home")
            {
                this.CurrentFolder = home;
            }
            else
            {
                this.CurrentFolder = this.CurrentFolder.GetFolder(name);
            }
        }

        public void GoToPreviousDirectory() // cd ..
        {
            if (this.CurrentFolder.ParentFolder != null)
            {
                this.CurrentFolder = this.CurrentFolder.ParentFolder;
            }
        }

        public int GetWordsCountInText(string text)
        {
            return text
                .Split(new string[] { " ", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .Length;
        }

        public int GetLinesCountInText(string text)
        {
            return text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }
}
