﻿namespace FileSystemTask
{
    public interface ISizable
    {
        string Name { get; }
        int Size { get; }
    }
}
