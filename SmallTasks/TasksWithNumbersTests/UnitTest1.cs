using SmallTasks;

namespace TasksWithNumbersTests
{
    [TestClass]
    public class UnitTest1
    {

        //  1. Test the limits of primitive types - add one to the Max number of a type and check result

        [TestMethod]
        public void TestIntMaxValueShouldReturnCorrectValue()
        {
            var expected = 2147483647;

            var actual = int.MaxValue;

            Assert.AreEqual(expected, actual);
        }

        // 2. 2. Odd number? bool IsOdd(int n)

        [TestMethod]
        public void TestIsOddShouldReturnTrueWithPositiveOddNumber()
        {
            var number = 7;

            var result = TasksWithNumbers.IsOdd(number);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestIsOddShouldReturnTrueWithNegativeOddNumber()
        {
            var number = -7;

            var result = TasksWithNumbers.IsOdd(number);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestIsOddShouldReturnFalseWithPositiveEvenNumber()
        {
            var number = 6;

            var result = TasksWithNumbers.IsOdd(number);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestIsOddShouldReturnFalseWithNegativeEvenNumber()
        {
            var number = -6;

            var result = TasksWithNumbers.IsOdd(number);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestIsOddShouldReturnFalseWithZero()
        {
            var number = 0;

            var result = TasksWithNumbers.IsOdd(number);

            Assert.IsFalse(result);
        }

        // 3. Prime number? bool IsPrime(int N)

        [TestMethod]
        public void TestIsPrimeShouldReturnFalseWithZero()
        {
            var number = 0;

            var result = TasksWithNumbers.IsPrime(0);

            Assert.IsFalse(result);
        }

        [TestMethod]

        public void TestIsPrimeShouldReturnFalseWithOne()
        {
            var number = 1;

            var result = TasksWithNumbers.IsPrime(1);

            Assert.IsFalse(result);
        }

        [TestMethod]
        [DataRow(2)]
        [DataRow(3)]
        [DataRow(5)]
        [DataRow(13)]
        [DataRow(97)]
        public void TestIsPrimeShouldReturnTrueWithPrimeNumbers(int primeNumber)
        {
            var result = TasksWithNumbers.IsPrime(primeNumber);

            Assert.IsTrue(result);
        }

        [TestMethod]
        [DataRow(-2)]
        [DataRow(-3)]
        [DataRow(-16)]
        [DataRow(-100)]
        public void TestIsPrimeShouldReturnFalseWithNegativeNumbers(int number)
        {
            var result = TasksWithNumbers.IsPrime(number);

            Assert.IsFalse(result);
        }

        // 4. Min element inside an array? int Min(int... array)

        [TestMethod]
        public void TestMinShouldReturnMinValueCorrectlyWithPositiveNumbers()
        {
            var numbers = new int[] { 1, 2, 3, 4, 7, 10 };

            var expected = 1;
            var actual = TasksWithNumbers.Min(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMinShouldReturnMinValueCorrectlyWithNegativeNumbers()
        {
            var numbers = new int[] { -1, -1000, -54, -86, -65 };

            var expected = -1000;
            var actual = TasksWithNumbers.Min(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMinShouldReturnMinValueCorrectly()
        {
            var numbers = new int[] { -589, -2562, 0, 4, 26, 116 };

            var expected = -2562;
            var actual = TasksWithNumbers.Min(numbers);

            Assert.AreEqual(expected, actual);
        }

        // 5. k-th min element inside an array? int KthMin(int k, int[] array)

        [TestMethod]
        public void TestKthMinShouldReturnCorrectResultWithPositiveNumbers()
        {
            var numbers = new int[] { 1, 2, 3, 4, 5, 6 };
            var k = 2;

            var expected = 3;
            var actual = TasksWithNumbers.KthMin(k, numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestKthMinShouldReturnCorrectResultWithNegativeNumbers()
        {
            var numbers = new int[] { -5, -100, -8, -2, -66 };
            var k = 0;

            var expected = -100;
            var actual = TasksWithNumbers.KthMin(k, numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestKthMinShouldWorkProperly()
        {
            var numbers = new int[] { -5, 80, 200, 0, -66 };
            var k = 1;

            var expected = -5;
            var actual = TasksWithNumbers.KthMin(k, numbers);

            Assert.AreEqual(expected, actual);
        }

        // 6.Find a number, that occurs odd times in an array int GetOddOccurrence(int... array)

        [TestMethod]
        public void TestGetOddOccurrenceShouldReturnTheFirstNumberThatOccurrsOnes()
        {
            var numbers = new int[] { 1, 2, 3, 4 };

            var expected = 1;
            var actual = TasksWithNumbers.GetOddOccurrence(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetOddOccurrenceShouldWorkProperly()
        {
            var numbers = new int[] { 5, 1, 2, 1, 2, 5, 5 };

            var expected = 5;
            var actual = TasksWithNumbers.GetOddOccurrence(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetOddOccurrenceShouldNotFoundNumberWithOddOccurrence()
        {
            var numbers = new int[] { 1, 12, 12, 1, 18, 18 };

            var expected = -1;
            var actual = TasksWithNumbers.GetOddOccurrence(numbers);

            Assert.AreEqual(expected, actual);
        }

        // 7. Average of elements inside that array? int GetAverage(int[] array);

        [TestMethod]
        [DataRow(new int[] { 1, 2, 3, 4, 5, 6, 7 })]
        [DataRow(new int[] { 1, 8, 17, 5, 14, 55, 4, 0 })]
        [DataRow(new int[] { -1, 0, 0, -1, 2 })]
        public void TestGetAverageShouldReturnCorrectResult(int[] array)
        {
            var expected = (int)array.Average();

            var actual = TasksWithNumbers.GetAverage(array);

            Assert.AreEqual(expected, actual);
        }

        // 8. Raise an integer to a power of another (Write a O(log(b)) solution) long pow(int a, int b)

        [TestMethod]
        public void TestPowShoulReturnCorrectValueWithPositiveNumbers()
        {
            int a = 2;
            int b = 3;

            var expected = (int)Math.Pow(a, b);
            var actual = TasksWithNumbers.Pow(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestPowShoulReturnOneWhenRaisedToAZero()
        {
            int a = 2;
            int b = 0;

            var expected = 1;
            var actual = TasksWithNumbers.Pow(a, b);

            Assert.AreEqual(expected, actual);
        }

        // 9. Smallest multiple of a number - a number that can be divided by each of the numbers from one to a given N long GetSmallestMultiple(int N);

        [TestMethod]
        public void TestGetSmallestMultipleShouldReturnCorrectValue()
        {
            var N = 10;

            var expected = 2520;
            var actual = TasksWithNumbers.GetSmallestMultiple(N);

            Assert.AreEqual(expected, actual);
        }

        // 10. Double factorial? long DoubleFac(int n); if n=3, => (3!)! = 6! = 720

        [TestMethod]
        public void TestDoubleFacShouldReturnOneWithZero()
        {
            var n = 0;

            var expected = 1;
            var actual = TasksWithNumbers.DoubleFac(n);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestDoubleFacShouldReturnCorrectResult()
        {
            var n = 3;

            var expected = 720;
            var actual = TasksWithNumbers.DoubleFac(n);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestDoubleFacShouldReturnCorrectResultWithTwo()
        {
            var n = 2;

            var expected = 2;
            var actual = TasksWithNumbers.DoubleFac(n);

            Assert.AreEqual(expected, actual);
        }

        // 11. k-th factorial? long KthFac(int k, int n);

        [TestMethod]
        public void TestKthFacShouldReturnOneWithZero()
        {
            var n = 0;
            var k = 4;

            var expected = 1;
            var actual = TasksWithNumbers.KthFac(k, n);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestKthFacShouldReturnCorrectResultWithTwo()
        {
            var n = 2;
            var k = 3;

            var expected = 2;
            var actual = TasksWithNumbers.KthFac(k, n);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestKthFacShouldReturnCorrectResult()
        {
            var n = 3;
            var k = 2;

            long expected = 720;
            var actual = TasksWithNumbers.KthFac(k, n);

            Assert.AreEqual(expected, actual);
        }

        // 12. maximal scalar product - by two vectors given a and b, return their scalar product (as a bonus, find a permutation of a and b where their scalar product is max) long MaximalScalarSum(int[] a, int[] b)

        [TestMethod]
        public void TestMaximalScalarSumShouldThrowAnExceptionWithDifferentArraysLength()
        {
            var a = new int[] { 1, 2, 3 };
            var b = new int[] { 1, 3, 4, 5 };

            Assert.ThrowsException<ArgumentException>(() => TasksWithNumbers.MaximalScalarSum(a, b));
        }

        [TestMethod]
        public void TestMaximalScalarSumShouldReturnMaxScalarSum()
        {
            var a = new int[] { 1, 2, 3 };
            var b = new int[] { 2, 3, 5 };

            var expected = 17;
            var actual = TasksWithNumbers.MaximalScalarSum(a, b);

            Assert.AreEqual(expected, actual);
        }

        // 13. max span - return the max number of elements (span) between a element from the left and from the right side of an array that is equal. A single number has a span of 1. int MaxSpan(int[] numbers) maxSpan(2, 5, 4, 1, 3, 4) ? 4 MaxSpan(8, 12, 7, 1, 7, 2, 12) ? 6 MaxSpan(3, 6, 6, 8, 4, 3, 6) ? 6


        [TestMethod]
        public void TestMaxSpanShouldReturnCorrectResult()
        {
            var firstTestNumbers = new int[] { 2, 5, 4, 1, 3, 4 };
            var secondTestNumbers = new int[] { 8, 12, 7, 1, 7, 2, 12 };
            var thirdTestNumbers = new int[] { 3, 6, 6, 8, 4, 3, 6 };

            Assert.AreEqual(4, TasksWithNumbers.MaxSpan(firstTestNumbers));
            Assert.AreEqual(6, TasksWithNumbers.MaxSpan(secondTestNumbers));
            Assert.AreEqual(6, TasksWithNumbers.MaxSpan(thirdTestNumbers));
        }

        // equal sum sides - return true if there is an element in the array, that you can remove and those from the left and right have the same sum bool EqualSumSides(int[] numbers) EqualSumSides(3, 0, -1, 2, 1) ? true EqualSumSides(2, 1, 2, 3, 1, 4) ? true EqualSumSides(8, 8) ? false

        [TestMethod]
        public void TestEqualSumSidesShouldWorkProperly()
        {
            var firstTestNumbers = new int[] { 3, 0, -1, 2, 1 };
            var secondTestNumbers = new int[] { 2, 1, 2, 3, 1, 4 };
            var thirdTestNumbers = new int[] { 8, 8 };

            Assert.IsTrue(TasksWithNumbers.EqualSumSides(firstTestNumbers));
            Assert.IsTrue(TasksWithNumbers.EqualSumSides(secondTestNumbers));
            Assert.IsFalse(TasksWithNumbers.EqualSumSides(thirdTestNumbers));
        }
    }
}