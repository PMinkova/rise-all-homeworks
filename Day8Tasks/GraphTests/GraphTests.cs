namespace GraphTests
{
    using Day8Tasks;

    [TestClass]
    public class GraphTests
    {
        [TestMethod]
        public void GraphHasCycleShoulReturnTrueForGraphWithCycle()
        {
            var graph = new Graph(6);

            graph.AddEgde(0, 3);
            graph.AddEgde(1, 0);
            graph.AddEgde(3, 2);
            graph.AddEgde(3, 4);
            graph.AddEgde(1, 2);
            graph.AddEgde(4, 5);

            Assert.IsTrue(graph.GraphHasCycle(0));
            Assert.IsTrue(graph.GraphHasCycle(1));
            Assert.IsTrue(graph.GraphHasCycle(2));
            Assert.IsTrue(graph.GraphHasCycle(3));
            Assert.IsTrue(graph.GraphHasCycle(4));
            Assert.IsTrue(graph.GraphHasCycle(5));
        }

        [TestMethod]
        public void GraphHasCycleShoulReturnFalseForNoCycle()
        {
            var graph = new Graph(6);

            graph.AddEgde(0, 3);
            graph.AddEgde(3, 2);
            graph.AddEgde(3, 4);
            graph.AddEgde(1, 2);
            graph.AddEgde(4, 5);

            Assert.IsFalse(graph.GraphHasCycle(0));
        }

        [TestMethod]
        public void GraphHasCycleShouldReturnTrue()
        {
            var graph = new Graph(5);

            graph.AddEgde(0, 1);
            graph.AddEgde(0, 2);
            graph.AddEgde(0, 3);
            graph.AddEgde(3, 4);
            graph.AddEgde(2, 3);

            bool result = graph.GraphHasCycle(0);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GraphHasCycleShouldReturnFalse()
        {
            var graph = new Graph(5);

            graph.AddEgde(0, 1);
            graph.AddEgde(0, 2);
            graph.AddEgde(0, 3);
            graph.AddEgde(3, 4);

            bool result = graph.GraphHasCycle(0);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GraphHasCycleShouldReturnFalseWithSixElements()
        {
            var graph = new Graph(6);

            graph.AddEgde(0, 3);
            graph.AddEgde(3, 2);
            graph.AddEgde(3, 4);
            graph.AddEgde(1, 2);
            graph.AddEgde(4, 5);

            bool result = graph.GraphHasCycle(0);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GraphHasCycleShouldReturnTrueWithBiggerGraph()
        {
            var graph = new Graph(8);

            graph.AddEgde(0, 1);
            graph.AddEgde(1, 2);
            graph.AddEgde(2, 3);
            graph.AddEgde(3, 4);
            graph.AddEgde(4, 0);
            graph.AddEgde(1, 5);
            graph.AddEgde(3, 6);
            graph.AddEgde(6, 7);

            bool result = graph.GraphHasCycle(0);

            Assert.IsTrue(result);
        }
    }
}