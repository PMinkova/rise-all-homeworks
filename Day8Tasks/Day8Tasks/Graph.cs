﻿namespace Day8Tasks
{

    public class Graph
    {
        private int vertexCount;
        private int[,] matrix;
        private int edgeCount;

        public Graph(int vertexCount)
        {
            this.vertexCount = vertexCount;

            matrix = new int[vertexCount, vertexCount];

            for (int i = 0; i < vertexCount; i++)
            {
                for (int j = 0; j < vertexCount; j++)
                {
                    matrix[i, j] = 0;
                }
            }
        }

        public void AddEgde(int start, int end)
        {
            matrix[start, end] = 1;
            matrix[end, start] = 1;
            this.edgeCount++;
        }

        public bool GraphHasCycle(int node)
        {
            var visited = new HashSet<int>();

            return this.HasCycle(node, visited);
        }

        private bool HasCycle(int node, HashSet<int> visited)
        {
            for (int i = 0; i < vertexCount; i++)
            {
                if (matrix[node, i] == 1 && visited.Contains(i) && visited.Last() != i)
                {
                    return true; 
                }
            }

            visited.Add(node);

            for (int i = 0; i < vertexCount; i++)
            {
                if (matrix[node, i] == 1 && !visited.Contains(i))
                {
                    if (HasCycle(i, visited))
                    {
                        return true; 
                    }
                }
            }

            visited.Remove(node);

            return false;
        }


        // Another variant of to check is a graph has a cycle
        public bool TestHasCycle(int node)
        {
            if (this.edgeCount < this.vertexCount)
            {
                return false;
            }

            return true;
        }
    }
}
