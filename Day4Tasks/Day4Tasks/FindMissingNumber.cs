﻿namespace Day4Tasks
{
    public class FindMissingNumber
    {
        public static int GetMissingNumber(int[] numbers)
        {
            if (numbers.Length < 2)
            {
                throw new ArgumentException("The array must contain at least 2 elements");
            }

            SortArray(numbers);

            for (int i = 0; i < numbers.Length - 1; i++)
            {
                int currentElement = numbers[i];
                int nextElement = numbers[i + 1];

                if (currentElement + 1 != nextElement)
                {
                    return currentElement + 1;
                }
            }

            throw new ArgumentException("No missing number found in the array.");
        }


        public static void SortArray(int[] numbers)
        {
            while (true)
            {
                var hasChanged = false;

                for (int i = 0; i < numbers.Length - 1; i++)
                {
                    var currentElement = numbers[i];
                    var nextElement = numbers[i + 1];

                    if (currentElement > nextElement)
                    {
                        var temp = currentElement;
                        numbers[i] = nextElement;
                        numbers[i + 1] = temp;
                        hasChanged = true;
                    }
                }

                if (hasChanged == false)
                {
                    return;
                }
            }
        }
    }
}
